
#include <vtkActor.h>
// #include <vtkCellArray.h>
// #include <vtkDoubleArray.h>
// #include <vtkFloatArray.h>
// #include <vtkIntArray.h>
// #include <vtkNamedColors.h>
#include <vtkNew.h>
// #include <vtkPointData.h>
// #include <vtkPoints.h>
// #include <vtkPolyData.h>
// #include <vtkPolyDataMapper.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
// #include <vtkInteractorStyleTrackballCamera.h>
#include <vtkRenderer.h>
#include <vtkCamera.h>
#include <vtkMatrix4x4.h>
#include <vtkWindowToImageFilter.h>
#include <vtkBMPWriter.h>
#include <vtkImageShiftScale.h>
#include <vtkTransform.h>

#include <string>

#include "vtkPointCloud.h"
#include "configReader.h"

using namespace std;


vtkSmartPointer<vtkCamera> makeVtkCamera(configReader* cameraConfig,
                                           double depthMin=0.1, double depthMax=3000.0)
{
    vtkNew<vtkCamera> camera;

    camera->SetPosition(0, 0, 0);
    camera->SetFocalPoint(0, 0, 1);
    camera->SetViewUp(0, -1, 0);

    camera->SetClippingRange(depthMin, depthMax);
    
    // convert the principal point to window center (normalized coordinate system) and set it
    double wcx = -2*(cameraConfig->get_cx() - double(cameraConfig->get_w())/2) / cameraConfig->get_w();
    double wcy =  2*(cameraConfig->get_cy() - double(cameraConfig->get_h())/2) / cameraConfig->get_h();
    // values are nagated to match with "desired output" from coding assignment
    camera->SetWindowCenter(-wcx, -wcy);

    cout << "wcx, wcy: " << wcx << ", " << wcy << "\n";

    // Set vertical view angle as a indirect way of setting the y focal distance
    double view_angle = 180.0 / M_PI * (2.0 * std::atan2( cameraConfig->get_h() / 2.0, cameraConfig->get_fy() ));
    std::cout << "view_angle = " << view_angle << std::endl;
    camera->SetViewAngle( view_angle );

    double aspect = cameraConfig->get_fy() / cameraConfig->get_fx();

    // camera->SetUseExplicitAspectRatio(true);
    // camera->SetExplicitAspectRatio(aspect);

    // Set the image aspect ratio as an indirect way of setting the x focal distance
    vtkNew<vtkMatrix4x4> mat;
    mat->SetElement(0, 0, 1.0 / aspect);
    mat->SetElement(1, 1, 1.0);
    mat->SetElement(2, 2, 1.0);
    mat->SetElement(3, 3, 1.0);
    vtkNew<vtkTransform> transform;
    transform->SetMatrix(mat);
    camera->SetUserTransform(transform);

    return camera;
}


int main(int argc, char* argv[])
{
    std::string inputFilename;
    std::string outputFilename;

    if (argc > 1)
    {
        inputFilename = argv[1];
    }
    else
    {
        inputFilename = "../../pointcloud.txt";
    }

    if (argc > 2)
    {
        outputFilename = argv[2];
    }
    else
    {
        outputFilename = "output.bmp";
    }

    vtkNew<vtkWindowToImageFilter> filter;
    vtkNew<vtkBMPWriter> imageWriter;
    vtkNew<vtkImageShiftScale> scale;
    
    vtkPointCloud* pc = new vtkPointCloud(inputFilename);

    configReader* cameraConfig = new configReader();

    vtkNew<vtkRenderer> ren;
    ren->AddActor(pc->actor);
    ren->SetActiveCamera(makeVtkCamera(cameraConfig, pc->getDepthMin(), pc->getDepthMax()));
    
    vtkNew<vtkRenderWindow> renWin;
    renWin->AddRenderer(ren);
    renWin->SetSize(cameraConfig->get_w(), cameraConfig->get_h());
    
    vtkNew<vtkRenderWindowInteractor> iren;
    iren->SetRenderWindow(renWin);
    renWin->Render();
    // iren->Initialize();
    // iren->Start();

    // Create Depth Map
    filter->SetInput(renWin);
#if VTK_MAJOR_VERSION >= 8 || VTK_MAJOR_VERSION == 8 && VTK_MINOR_VERSION >= 90
    filter->SetScale(1);
#else
    filter->SetMagnification(1);
#endif
    filter->SetInputBufferTypeToZBuffer();

    scale->SetOutputScalarTypeToUnsignedChar();
    scale->SetInputConnection(filter->GetOutputPort());
    scale->SetShift(0);
    scale->SetScale(-255);

    imageWriter->SetFileName(outputFilename.c_str());
    imageWriter->SetInputConnection(scale->GetOutputPort());
    imageWriter->Write();

    return EXIT_SUCCESS;
}
