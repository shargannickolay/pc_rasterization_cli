
#include "vtkPointCloud.h"

std::vector<std::string> split(std::string str, char delimiter) { 
    std::vector<std::string> internal; 
    std::stringstream ss(str);
    std::string tok; 

    while(getline(ss, tok, delimiter)) { 
        internal.push_back(tok); 
    } 

    return internal; 
} 

vtkPointCloud::vtkPointCloud(int zMin, int zMax){
    
    clearPoints();

    mapper->SetInputData(polydata);
    mapper->SetScalarRange(zMin, zMax);
    mapper->SetScalarVisibility(1);

    actor->SetMapper(mapper);
}

vtkPointCloud::vtkPointCloud(std::string filepath, int zMin, int zMax){
    
    clearPoints();

    mapper->SetInputData(polydata);
    mapper->SetScalarRange(zMin, zMax);
    mapper->SetScalarVisibility(1);

    actor->SetMapper(mapper);

    fstream newfile;
    std::vector<std::vector<double>> coords;
    
    newfile.open(filepath, ios::in);
    if (newfile.is_open()){
        std::string tp;
        while(getline(newfile, tp)){
            std::vector<double> coordsCurr;
            try{
                coordsCurr.push_back(std::stod(split(tp, ' ')[0]));
                 // minus added to align with expected results
                coordsCurr.push_back(-std::stod(split(tp, ' ')[1]));
                coordsCurr.push_back(std::stod(split(tp, ' ')[2]));
                
                coords.push_back(coordsCurr);
            } catch (...) {
                break;
            }
        }
        newfile.close();
    }

    cout << "points num: " << coords.size() << "\n";
    cout << "point_xyz[0]: " << coords[0][0] << ", " << coords[0][1] << ", " << coords[0][2] << "\n";

    for (auto pointIdx = 0; pointIdx < coords.size(); pointIdx++){
        addPoint(coords[pointIdx]);
    }
}

void vtkPointCloud::addPoint(std::vector<double> pcoordsIn){
    int pointId = points->InsertNextPoint(pcoordsIn[0], pcoordsIn[1], pcoordsIn[2]);
    pcoords->InsertNextValue(pcoordsIn[2]);
    strips->InsertNextCell(1);
    strips->InsertCellPoint(pointId);

    strips->Modified();
    pcoords->Modified();
    points->Modified();
} 

void vtkPointCloud::clearPoints(){
    pcoords->SetName("DepthArray");
    polydata->SetPoints(points);
    polydata->SetVerts(strips);
    polydata->GetPointData()->SetScalars(pcoords);
    // polydata->GetPointData()->SetActiveScalars('DepthArray');
}

double vtkPointCloud::getDepthMin(){
    if (points->GetNumberOfPoints() == 0){
        cout << "points were not added";
        return -1.0;
    }
    double pcoords[3];
    points->GetPoint(0, pcoords);

    double depthMin = pcoords[2];
    
    for (vtkIdType pointIdx = 1; pointIdx < points->GetNumberOfPoints(); ++pointIdx)
    {
        points->GetPoint(pointIdx, pcoords);
        
        if (pcoords[2] < depthMin){
            depthMin = pcoords[2];
        }
    }

    return depthMin;
}

double vtkPointCloud::getDepthMax(){
    if (points->GetNumberOfPoints() == 0){
        cout << "points were not added";
        return -1.0;
    }
    double pcoords[3];
    points->GetPoint(0, pcoords);

    double depthMax = pcoords[2];
    
    for (vtkIdType pointIdx = 1; pointIdx < points->GetNumberOfPoints(); ++pointIdx)
    {
        points->GetPoint(pointIdx, pcoords);
        
        if (pcoords[2] > depthMax){
            depthMax = pcoords[2];
        }
    }

    return depthMax;
}