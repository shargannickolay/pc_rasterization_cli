
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#ifndef CONFIGREADER_H
#define CONFIGREADER_H

class configReader{
    double fx = 2172.1;
    double fy = 2171.6;
    double cx = 960.8;
    double cy = 602.5;
    int     w = 1440;
    int     h = 900;

public:
    configReader(std::string filepath = "../config.txt");

    double get_fx();
    double get_fy();
    double get_cx();
    double get_cy();
    int    get_w();
    int    get_h();
};

#endif