# pc_rasterization_cli

## Installation
```bash
mkdir build
cd build
cmake ..
make
```
## Usage
./pcRasterizator <path_to_pc_txtfile> <output_image_path.bmp>