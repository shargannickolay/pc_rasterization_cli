#include "configReader.h"

configReader::configReader(std::string filepath){
    std::fstream newfile;
    std::vector<std::vector<double>> coords;
    
    newfile.open(filepath, std::ios::in);
    if (newfile.is_open()){
        std::string tp;
        try{
            getline(newfile, tp);
            fx = std::stod(tp);
            getline(newfile, tp);
            fy = std::stod(tp);
            getline(newfile, tp);
            cx = std::stod(tp);
            getline(newfile, tp);
            cy = std::stod(tp);
            getline(newfile, tp);
            w = std::stoi(tp);
            getline(newfile, tp);
            h = std::stoi(tp);
        } catch (...) {
            return;
        }

        newfile.close();
    }

}

double configReader::get_fx(){
    return fx;
}

double configReader::get_fy(){
    return fy;
}

double configReader::get_cx(){
    return cx;
}

double configReader::get_cy(){
    return cy;
}

int configReader::get_w(){
    return w;
}

int configReader::get_h(){
    return h;
}