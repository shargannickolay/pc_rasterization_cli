
#include <vtkActor.h>
#include <vtkCellArray.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkIntArray.h>
#include <vtkNew.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#ifndef VTKPOINTCLOUD_H
#define VTKPOINTCLOUD_H

class vtkPointCloud{
    vtkNew<vtkPolyDataMapper> mapper;
    vtkNew<vtkPoints> points;
    vtkNew<vtkCellArray> strips;
    vtkNew<vtkPolyData> polydata;
    vtkNew<vtkDoubleArray> pcoords;

    void clearPoints();
public:
    vtkNew<vtkActor> actor;
    vtkPointCloud(int zMin=-10.0, int zMax=10.0);
    vtkPointCloud(std::string filepath, int zMin=-10.0, int zMax=10.0);
    void addPoint(std::vector<double> pcoordsIn);
    double getDepthMin();
    double getDepthMax();
};

#endif